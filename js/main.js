var bars = document.querySelectorAll('.table__bar-count');
for(var i=0,item; item=bars[i++];) {
    item.addEventListener('mouseover', function() {
        this.parentNode.parentNode.parentNode
            .querySelector('.table__rate')
            .innerHTML = parseInt(this.style.height);
    });

    item.addEventListener('mouseout', function() {
        var currentItem = this.parentNode.querySelector('.table__bar-count_current');
        this.parentNode.parentNode.parentNode
            .querySelector('.table__rate')
            .innerHTML = parseInt(currentItem.style.height)+
            (
                currentItem.hasAttribute('data-compare')
                    ?
                        '<span class="table__rate-'+(parseFloat(currentItem.getAttribute('data-compare'))>0?'plus':'minus')+
                        '">'+String(Math.abs(parseFloat(currentItem.getAttribute('data-compare')))).replace('.',',')+
                        '</span>'
                    :
                        ''
            );
    });
}
